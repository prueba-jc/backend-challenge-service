package com.seera.backendchallengeservice.client.providerb;

import com.seera.backendchallengeservice.core.model.Hotel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class ProviderBClientTest {

    @InjectMocks
    private ProviderBClient providerBClient;

    @Test
    public void getHotelListReturnHotelsFromProviderB(){
        List<Hotel> hotelList = providerBClient.getHotelList();

        assertNotNull(hotelList);
        assertFalse(hotelList.isEmpty());
        assertEquals(990, hotelList.size());
        assertEquals(980, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 1).count());
        assertEquals(10, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 2).count());
    }
}