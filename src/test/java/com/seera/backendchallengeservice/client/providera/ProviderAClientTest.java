package com.seera.backendchallengeservice.client.providera;

import com.seera.backendchallengeservice.core.model.Hotel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class ProviderAClientTest {

    @InjectMocks
    private ProviderAClient providerAClient;

    @Test
    public void getHotelListReturnHotelsFromProviderA(){
        List<Hotel> hotelList = providerAClient.getHotelList();

        assertNotNull(hotelList);
        assertFalse(hotelList.isEmpty());
        assertEquals(168, hotelList.size());
        assertEquals(22, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 0).count());
        assertEquals(46, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 1).count());
        assertEquals(47, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 2).count());
        assertEquals(32, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 3).count());
        assertEquals(13, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 4).count());
        assertEquals(5, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 5).count());
        assertEquals(2, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 6).count());
        assertEquals(0, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 7).count());
        assertEquals(0, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 8).count());
        assertEquals(1, hotelList.stream().filter(hotel -> hotel.getRooms().size() == 9).count());
    }
}