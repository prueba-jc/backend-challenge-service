package com.seera.backendchallengeservice.ws.controller;

import com.seera.backendchallengeservice.core.SearcherService;
import com.seera.backendchallengeservice.core.model.Search;
import com.seera.backendchallengeservice.ws.model.SearchDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.core.convert.ConversionService;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyFloat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(SearcherController.class)
public class SearcherControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private SearcherService searcherService;
    @Mock
    private ConversionService conversionService;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new SearcherController(searcherService, conversionService))
                .alwaysExpect(forwardedUrl(null))
                .build();
    }

    @Test
    public void searchHotelAndRoomsShouldAcceptNullValues() throws Exception {
        Search search = Search.builder().build();
        when(searcherService.searchHotelsAndRooms(null, null, null)).thenReturn(search);

        SearchDTO searchDTO = SearchDTO.builder()
                .hotels(new ArrayList<>())
                .build();
        when(conversionService.convert(search, SearchDTO.class)).thenReturn(searchDTO);

        this.mockMvc.perform(get("/searcher/hotels/room/search"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.hotels").isArray());

        verify(searcherService, times(1)).searchHotelsAndRooms(null, null, null);
        verify(conversionService, times(1)).convert(any(), any());
    }

    @Test
    public void searchHotelAndRoomsWithValuesReturnsOk() throws Exception {
        Search search = Search.builder().build();
        when(searcherService.searchHotelsAndRooms("Bethesda", 2, Float.valueOf("10.05"))).thenReturn(search);

        SearchDTO searchDTO = SearchDTO.builder()
                .hotels(new ArrayList<>())
                .build();
        when(conversionService.convert(search, SearchDTO.class)).thenReturn(searchDTO);

        this.mockMvc.perform(get("/searcher/hotels/room/search?location=Bethesda&size=2&rate=10.05"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.hotels").isArray());

        verify(searcherService, times(1)).searchHotelsAndRooms(anyString(), anyInt(), anyFloat());
        verify(conversionService, times(1)).convert(any(), any());
    }

    @Test
    public void searchHotelAndRoomsWithBadIntegerShouldReturnBadRequest() throws Exception {
        this.mockMvc.perform(get("/searcher/hotels/room/search?location=Bethesda&size=notInteger&rate=10.05"))
                .andDo(print())
                .andExpect(status().isBadRequest());

        verify(searcherService, times(0)).searchHotelsAndRooms(anyString(), anyInt(), anyFloat());
        verify(conversionService, times(0)).convert(any(), any());
    }

    @Test
    public void searchHotelAndRoomsWithBadFloatShouldReturnBadRequest() throws Exception {
        this.mockMvc.perform(get("/searcher/hotels/room/search?location=Bethesda&size=2&rate=notFloat"))
                .andDo(print())
                .andExpect(status().isBadRequest());

        verify(searcherService, times(0)).searchHotelsAndRooms(anyString(), anyInt(), anyFloat());
        verify(conversionService, times(0)).convert(any(), any());
    }
}