package com.seera.backendchallengeservice.ws.mapper;

import com.seera.backendchallengeservice.core.model.Search;
import com.seera.backendchallengeservice.ws.model.SearchDTO;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SearchToSearchDTOMapperTest {

    private final ResponseToResponseDTOMapper responseToResponseDTOMapper = new ResponseToResponseDTOMapperImpl(new HotelToHotelDTOMapperImpl(null));

    @Test
    public void convertShouldReturnPriceDTO() {
        Search search = Search.builder()
                .hotels(Collections.emptyList())
                .build();

        SearchDTO searchDTO = responseToResponseDTOMapper.convert(search);

        assertNotNull(searchDTO);
    }
}