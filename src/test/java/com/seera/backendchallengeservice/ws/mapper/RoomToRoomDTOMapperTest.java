package com.seera.backendchallengeservice.ws.mapper;

import com.seera.backendchallengeservice.core.model.Room;
import com.seera.backendchallengeservice.ws.model.RoomDTO;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class RoomToRoomDTOMapperTest {

    private final RoomToRoomDTOMapper roomToRoomDTOMapper = Mappers.getMapper(RoomToRoomDTOMapper.class);

    @Test
    public void convertShouldReturnPriceDTO() {
        Room room = new Room("roomName", 5, (float) 22.50);

        RoomDTO roomDTO = roomToRoomDTOMapper.convert(room);

        assertNotNull(roomDTO);
        assertEquals(room.getName(), roomDTO.getName());
        assertEquals(room.getSize(), roomDTO.getSize());
        assertEquals(room.getRate(), roomDTO.getRate());
    }
}