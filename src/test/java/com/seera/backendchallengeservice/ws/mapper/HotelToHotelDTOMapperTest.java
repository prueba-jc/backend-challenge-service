package com.seera.backendchallengeservice.ws.mapper;

import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.ws.model.HotelDTO;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class HotelToHotelDTOMapperTest {

    private final HotelToHotelDTOMapper hotelToHotelDTOMapper = new HotelToHotelDTOMapperImpl(new RoomToRoomDTOMapperImpl());

    @Test
    public void convertShouldReturnPriceDTO() {
        Hotel hotel = new Hotel("hotelName", "Mallorca",Collections.emptyList());

        HotelDTO hotelDTO = hotelToHotelDTOMapper.convert(hotel);

        assertNotNull(hotelDTO);
        assertEquals(hotel.getName(), hotelDTO.getName());
        assertEquals(hotel.getLocation(), hotelDTO.getLocation());
        assertEquals(hotel.getRooms(), hotelDTO.getRooms());
    }
}