package com.seera.backendchallengeservice.core;

import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.core.model.Room;
import com.seera.backendchallengeservice.core.model.Search;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearcherServiceImplTest {

    @InjectMocks
    private SearcherServiceImpl searcherService;

    @Mock
    private HotelRetrieverService hotelRetrieverService;

    @Before
    public void loadHotelList(){
        List<Hotel> hotelList = Arrays.asList(
                new Hotel("H1", "Mallorca", Arrays.asList(new Room("R1", 1, Float.valueOf("10.5")), new Room("R2", 2, Float.valueOf("20.5")))),
                new Hotel("H2", "Menorca", Collections.singletonList(new Room("R1", 3, Float.valueOf("15.0")))),
                new Hotel("H3", "Ibiza", Arrays.asList(new Room("R1", 1, Float.valueOf("20.5")), new Room("R2", 2, Float.valueOf("30.5")))),
                new Hotel("H4", "Mallorca", Arrays.asList(new Room("R1", 1, Float.valueOf("21.15")), new Room("R2", 3, Float.valueOf("63.18")), new Room("R3", 4, Float.valueOf("80.24")))));
        when(hotelRetrieverService.getHotelListFromProviders()).thenReturn(hotelList);
    }

    @Test
    public void searchHotelsAndRoomsWithoutParamsReturnFullHotelsAndRooms() {
        Search search = searcherService.searchHotelsAndRooms(null, null, null);

        assertNotNull(search);
        assertFalse(search.getHotels().isEmpty());
        assertEquals(4, search.getHotels().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H1")));
        assertEquals(2, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H1")).findFirst().get().getRooms().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H2")));
        assertEquals(1, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H2")).findFirst().get().getRooms().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H3")));
        assertEquals(2, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H3")).findFirst().get().getRooms().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H4")));
        assertEquals(3, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H4")).findFirst().get().getRooms().size());
    }

    @Test
    public void searchHotelsAndRoomsReturnEmptyListWhenNoHotelsInLocation() {
        Search search = searcherService.searchHotelsAndRooms("testLocation", null, null);

        assertNotNull(search);
        assertTrue(search.getHotels().isEmpty());
    }

    @Test
    public void searchHotelsAndRoomsReturnEmptyListWhenNoRoomsWithSize() {
        Search search = searcherService.searchHotelsAndRooms(null, 5, null);

        assertNotNull(search);
        assertTrue(search.getHotels().isEmpty());
    }

    @Test
    public void searchHotelsAndRoomsReturnEmptyListWhenNoRoomsWithRate() {
        Search search = searcherService.searchHotelsAndRooms(null, null, Float.valueOf("0.0"));

        assertNotNull(search);
        assertTrue(search.getHotels().isEmpty());
    }

    @Test
    public void searchHotelsAndRoomsReturnHotelListFromLocation() {
        Search search = searcherService.searchHotelsAndRooms("Mallorca", null, null);

        assertNotNull(search);
        assertFalse(search.getHotels().isEmpty());
        assertEquals(2, search.getHotels().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H1")));
        assertEquals(2, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H1")).findFirst().get().getRooms().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H4")));
        assertEquals(3, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H4")).findFirst().get().getRooms().size());
    }

    @Test
    public void searchHotelsAndRoomsReturnRoomsWithDefinedSize() {
        Search search = searcherService.searchHotelsAndRooms(null, 3, null);

        assertNotNull(search);
        assertFalse(search.getHotels().isEmpty());
        assertEquals(2, search.getHotels().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H2")));
        assertEquals("R1", search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H2")).findFirst().get().getRooms().get(0).getName());
        assertEquals(1, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H2")).findFirst().get().getRooms().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H4")));
        assertEquals(1, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H4")).findFirst().get().getRooms().size());
        assertEquals("R2", search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H4")).findFirst().get().getRooms().get(0).getName());
    }

    @Test
    public void searchHotelsAndRoomsReturnRoomsWithDefinedRate() {
        Search search = searcherService.searchHotelsAndRooms(null, null, Float.valueOf("20.5"));

        assertNotNull(search);
        assertFalse(search.getHotels().isEmpty());
        assertEquals(2, search.getHotels().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H1")));
        assertEquals(1, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H1")).findFirst().get().getRooms().size());
        assertEquals("R2", search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H1")).findFirst().get().getRooms().get(0).getName());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H3")));
        assertEquals(1, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H3")).findFirst().get().getRooms().size());
        assertEquals("R1", search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H3")).findFirst().get().getRooms().get(0).getName());
    }

    @Test
    public void searchHotelsAndRoomsWithAllParamsReturnDefinedHotelAndRoom() {
        Search search = searcherService.searchHotelsAndRooms("Mallorca", 1, Float.valueOf("10.5"));

        assertNotNull(search);
        assertFalse(search.getHotels().isEmpty());
        assertEquals(1, search.getHotels().size());
        assertTrue(search.getHotels().stream().anyMatch(hotel -> hotel.getName().equalsIgnoreCase("H1")));
        assertEquals(1, search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H1")).findFirst().get().getRooms().size());
        assertEquals("R1", search.getHotels().stream().filter(hotel -> hotel.getName().equalsIgnoreCase("H1")).findFirst().get().getRooms().get(0).getName());
    }

}