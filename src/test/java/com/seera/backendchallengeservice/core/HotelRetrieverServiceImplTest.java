package com.seera.backendchallengeservice.core;

import com.seera.backendchallengeservice.client.providera.ProviderAClient;
import com.seera.backendchallengeservice.client.providerb.ProviderBClient;
import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.core.model.Room;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HotelRetrieverServiceImplTest {

    @InjectMocks
    private HotelRetrieverServiceImpl hotelRetrieverService;

    @Mock
    private ProviderAClient providerAClient;
    @Mock
    private ProviderBClient providerBClient;

    @Test
    public void getHotelListFromProvidersReturnsMergedHotelList() {
        List<Hotel> providerAHotelList = Arrays.asList(
                new Hotel("Hotel 1", "location test", Collections.singletonList(new Room())),
                new Hotel("Hotel 3", "location test", Collections.singletonList(new Room()))
        );
        when(providerAClient.getHotelList()).thenReturn(providerAHotelList);

        List<Hotel> providerBHotelList = Arrays.asList(
                new Hotel("Hotel 2", "location test", Collections.singletonList(new Room())),
                new Hotel("Hotel 4", "location test", Collections.singletonList(new Room())),
                new Hotel("Hotel 5", "location test", Collections.singletonList(new Room()))
        );
        when(providerBClient.getHotelList()).thenReturn(providerBHotelList);

        List<Hotel> hotelList = hotelRetrieverService.getHotelListFromProviders();

        assertNotNull(hotelList);
        assertFalse(hotelList.isEmpty());
        assertEquals(5, hotelList.size());
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 1")));
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 2")));
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 3")));
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 4")));
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 5")));
    }

    @Test
    public void getHotelListFromProvidersReturnsHotelListWithRooms() {
        List<Hotel> providerAHotelList = Arrays.asList(
                new Hotel("Hotel 1", "location test", Collections.singletonList(new Room())),
                new Hotel("Hotel 3", "location test", Collections.emptyList())
        );
        when(providerAClient.getHotelList()).thenReturn(providerAHotelList);

        List<Hotel> providerBHotelList = Arrays.asList(
                new Hotel("Hotel 2", "location test", Collections.singletonList(new Room())),
                new Hotel("Hotel 4", "location test", Collections.emptyList()),
                new Hotel("Hotel 5", "location test", Collections.singletonList(new Room()))
        );
        when(providerBClient.getHotelList()).thenReturn(providerBHotelList);

        List<Hotel> hotelList = hotelRetrieverService.getHotelListFromProviders();

        assertNotNull(hotelList);
        assertFalse(hotelList.isEmpty());
        assertEquals(3, hotelList.size());
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 1")));
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 2")));
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 5")));
    }

    @Test
    public void getHotelListFromProvidersWithDuplicatesMergeRooms() {
        List<Hotel> providerAHotelList = Arrays.asList(
                new Hotel("Hotel 1", "location test", Collections.singletonList(new Room("1", 2, null))),
                new Hotel("Hotel 3", "location test", Collections.singletonList(new Room("2", 2, null))),
                new Hotel("Hotel 4", "location test", Collections.singletonList(new Room("3", 2, null)))
        );
        when(providerAClient.getHotelList()).thenReturn(providerAHotelList);

        List<Hotel> providerBHotelList = Arrays.asList(
                new Hotel("Hotel 1", "location test", Collections.singletonList(new Room("4", 2, null))),
                new Hotel("Hotel 4", "location test", Collections.singletonList(new Room("5", 2, null))),
                new Hotel("Hotel 5", "location test", Collections.singletonList(new Room("6", 2, null)))
        );
        when(providerBClient.getHotelList()).thenReturn(providerBHotelList);

        List<Hotel> hotelList = hotelRetrieverService.getHotelListFromProviders();

        assertNotNull(hotelList);
        assertFalse(hotelList.isEmpty());
        assertEquals(4, hotelList.size());
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 1")));
        assertEquals(2, hotelList.stream().filter(hotel -> hotel.getName().equals("Hotel 1")).findFirst().get().getRooms().size());
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 3")));
        assertEquals(1, hotelList.stream().filter(hotel -> hotel.getName().equals("Hotel 3")).findFirst().get().getRooms().size());
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 4")));
        assertEquals(2, hotelList.stream().filter(hotel -> hotel.getName().equals("Hotel 4")).findFirst().get().getRooms().size());
        assertTrue(hotelList.stream().anyMatch(hotel -> hotel.getName().equals("Hotel 5")));
        assertEquals(1, hotelList.stream().filter(hotel -> hotel.getName().equals("Hotel 5")).findFirst().get().getRooms().size());
    }

}