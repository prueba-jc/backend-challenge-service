package com.seera.backendchallengeservice.ws.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Builder
@ToString
@Schema
public class HotelDTO implements Serializable {
    @Schema(description = "Hotel name")
    private String name;
    @Schema(description = "Hotel location")
    private String location;
    @Schema(description = "List with hotel rooms")
    private List<RoomDTO> rooms;
}
