package com.seera.backendchallengeservice.ws.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

@Getter
@Builder
@ToString
@Schema
public class SearchDTO implements Serializable {
    @Schema(description = "Hotel list")
    private List<HotelDTO> hotels;
}
