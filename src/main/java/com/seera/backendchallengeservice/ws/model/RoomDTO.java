package com.seera.backendchallengeservice.ws.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Builder
@ToString
@Schema
public class RoomDTO implements Serializable {
    @Schema(description = "Room name")
    private String name;
    @Schema(description = "Number of available people in the room")
    private int size;
    @Schema(description = "Room price")
    private Float rate;
}
