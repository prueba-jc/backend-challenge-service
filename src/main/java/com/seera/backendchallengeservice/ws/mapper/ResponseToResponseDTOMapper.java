package com.seera.backendchallengeservice.ws.mapper;

import com.seera.backendchallengeservice.core.model.Search;
import com.seera.backendchallengeservice.ws.model.SearchDTO;
import org.mapstruct.Mapper;
import org.springframework.core.convert.converter.Converter;

@Mapper(uses = HotelToHotelDTOMapper.class)
public interface ResponseToResponseDTOMapper extends Converter<Search, SearchDTO> {
    @Override
    SearchDTO convert(Search search);
}
