package com.seera.backendchallengeservice.ws.mapper;

import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.ws.model.HotelDTO;
import org.mapstruct.Mapper;
import org.springframework.core.convert.converter.Converter;

@Mapper(uses = RoomToRoomDTOMapper.class)
public interface HotelToHotelDTOMapper extends Converter<Hotel, HotelDTO> {
    @Override
    HotelDTO convert(Hotel hotel);
}
