package com.seera.backendchallengeservice.ws.mapper;

import com.seera.backendchallengeservice.core.model.Room;
import com.seera.backendchallengeservice.ws.model.RoomDTO;
import org.mapstruct.Mapper;
import org.springframework.core.convert.converter.Converter;

@Mapper
public interface RoomToRoomDTOMapper extends Converter<Room, RoomDTO> {
    @Override
    RoomDTO convert(Room room);
}
