package com.seera.backendchallengeservice.ws.controller;

import com.seera.backendchallengeservice.core.SearcherService;
import com.seera.backendchallengeservice.core.model.Search;
import com.seera.backendchallengeservice.ws.model.SearchDTO;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/searcher")
@RequiredArgsConstructor
public class SearcherController {

    private final SearcherService searcherService;
    private final ConversionService conversionService;

    @GetMapping(value = "/hotels/room/search")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = SearchDTO.class))),
            @ApiResponse(responseCode = "400")
    })
    public ResponseEntity<SearchDTO> searchHotelAndRooms(
            @Parameter(example = "Bethesda") @RequestParam(name = "location", required = false) String location,
            @Parameter(example = "2") @RequestParam(name = "size", required = false) Integer size,
            @Parameter(example = "10.05") @RequestParam(name = "rate", required = false) Float rate) {
        log.info("Request - location={}, size={}, rate={}", location, size, rate);

        Search search = searcherService.searchHotelsAndRooms(location, size, rate);

        log.info("Response - {}", search.toString());
        return ResponseEntity.ok(conversionService.convert(search, SearchDTO.class));
    }

}
