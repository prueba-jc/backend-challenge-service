package com.seera.backendchallengeservice.core;

import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.core.model.Room;
import com.seera.backendchallengeservice.core.model.Search;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SearcherServiceImpl implements SearcherService {
    private final HotelRetrieverService hotelRetrieverService;

    @Override
    public Search searchHotelsAndRooms(String location, Integer size, Float rate) {
        List<Hotel> hotelProviderList = hotelRetrieverService.getHotelListFromProviders();
        List<Hotel> finalHotelList = searchHotels(location, size, rate, hotelProviderList);

        return Search.builder().hotels(finalHotelList).build();
    }

    private List<Hotel> searchHotels(String location, Integer size, Float rate, List<Hotel> hotelProviderList) {
        return hotelProviderList.stream()
                .filter(hotel -> filterByLocation(location, hotel))
                .peek(hotel -> hotel.setRooms(searchRooms(size, rate, hotel)))
                .filter(hotel -> !hotel.getRooms().isEmpty())
                .collect(Collectors.toList());
    }

    private boolean filterByLocation(String location, Hotel hotel) {
        return location == null || hotel.getLocation().equalsIgnoreCase(location);
    }

    private List<Room> searchRooms(Integer size, Float rate, Hotel hotel) {
        return hotel.getRooms().stream()
                .filter(room -> filterBySize(size, room))
                .filter(room -> filterByRate(rate, room))
                .collect(Collectors.toList());
    }

    private boolean filterByRate(Float rate, Room room) {
        return rate == null || room.getRate().equals(rate);
    }

    private boolean filterBySize(Integer size, Room room) {
        return size == null || room.getSize() == size;
    }
}
