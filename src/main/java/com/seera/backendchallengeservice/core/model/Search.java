package com.seera.backendchallengeservice.core.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@Builder
public class Search {
    private List<Hotel> hotels;
}
