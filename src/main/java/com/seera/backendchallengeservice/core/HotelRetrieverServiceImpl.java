package com.seera.backendchallengeservice.core;

import com.seera.backendchallengeservice.client.providera.ProviderAClient;
import com.seera.backendchallengeservice.client.providera.model.Rate;
import com.seera.backendchallengeservice.client.providerb.ProviderBClient;
import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.core.model.Room;
import com.seera.backendchallengeservice.core.model.Search;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Service
public class HotelRetrieverServiceImpl implements HotelRetrieverService {
    private final ProviderAClient providerAClient;
    private final ProviderBClient providerBClient;

    @Override
    @Cacheable("hotelsFromProvider")
    public List<Hotel> getHotelListFromProviders() {
        List<Hotel> providerAHotelList = providerAClient.getHotelList();
        List<Hotel> providerBHotelList = providerBClient.getHotelList();

        return mergeHotels(providerAHotelList, providerBHotelList);
    }

    @SafeVarargs
    private List<Hotel> mergeHotels(List<Hotel>... hotelsList) {
        Map<String, Hotel> hotelMap = Stream.of(hotelsList)
               .flatMap(List::stream)
               .filter(this::isHotelWithRooms)
               .collect(Collectors.toMap(
                       Hotel::getName,
                       Function.identity(),
                       (hotel1, hotel2) -> {
                           hotel1.setRooms(mergeRooms(hotel1, hotel2));
                           return hotel1;
                       }));
        return new ArrayList<>(hotelMap.values());
    }

    /**
     * <b>Note:</b> Duplicated rooms are not contemplated.
     */
    private List<Room> mergeRooms(Hotel hotel1, Hotel hotel2) {
        return Stream.of(hotel1.getRooms(), hotel2.getRooms())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private boolean isHotelWithRooms(Hotel hotel) {
        return !hotel.getRooms().isEmpty();
    }
}
