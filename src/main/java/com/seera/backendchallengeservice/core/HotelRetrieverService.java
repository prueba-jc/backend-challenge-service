package com.seera.backendchallengeservice.core;

import com.seera.backendchallengeservice.core.model.Hotel;

import java.util.List;

public interface HotelRetrieverService {
    List<Hotel> getHotelListFromProviders();
}
