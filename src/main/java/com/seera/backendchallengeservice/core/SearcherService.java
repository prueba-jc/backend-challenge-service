package com.seera.backendchallengeservice.core;

import com.seera.backendchallengeservice.core.model.Search;

public interface SearcherService {
    Search searchHotelsAndRooms(String location, Integer size, Float rate);
}
