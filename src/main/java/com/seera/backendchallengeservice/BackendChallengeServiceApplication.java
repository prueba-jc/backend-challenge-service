package com.seera.backendchallengeservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class BackendChallengeServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendChallengeServiceApplication.class, args);
	}

}
