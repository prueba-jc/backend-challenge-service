package com.seera.backendchallengeservice.client.providerb.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Rate {
    private String city;
    private String hotel;
    private String room;
    private int size;
    private Float rate;
}
