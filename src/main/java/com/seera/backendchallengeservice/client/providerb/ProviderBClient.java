package com.seera.backendchallengeservice.client.providerb;

import com.seera.backendchallengeservice.client.ExternalClient;
import com.seera.backendchallengeservice.client.providerb.model.Rate;
import com.seera.backendchallengeservice.client.utils.FileReaderUtils;
import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.core.model.Room;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class ProviderBClient implements ExternalClient {
    @Override
    public List<Hotel> getHotelList() {
        List<Rate> rateList =
                FileReaderUtils.readList("client/providerB/rates.json", Rate.class);

       /* Map<String, List<Rate>> rateMap = rateList.stream()
                .collect(
                        Collectors.groupingBy(Rate::getHotel, HashMap::new, Collectors.toCollection(ArrayList::new))
                );
        log.info("RateMap size: {}",rateMap.size());
*/
        return convertClientResponseToHotelList(rateList);
    }

    private List<Hotel> convertClientResponseToHotelList(List<Rate> rateList) {
        Map<String, Hotel> hotelHashMap = new HashMap<>();
        for (Rate rate : rateList){
            Hotel hotel;
            if (hotelHashMap.containsKey(rate.getHotel())){
                hotel = hotelHashMap.get(rate.getHotel());
            } else {
                hotel = new Hotel(rate.getHotel(), rate.getCity(), new ArrayList<>());
                hotelHashMap.put(rate.getHotel(), hotel);
            }
            hotel.getRooms().add(new Room(rate.getRoom(), rate.getSize(), rate.getRate()));
        }
        return new ArrayList<>(hotelHashMap.values());
    }

}
