package com.seera.backendchallengeservice.client;

import com.seera.backendchallengeservice.core.model.Hotel;

import java.util.List;

public interface ExternalClient {
    List<Hotel> getHotelList();
}
