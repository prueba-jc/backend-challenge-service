package com.seera.backendchallengeservice.client.providera.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class HotelProvider {
    @JsonProperty("ID")
    private int id;
    @JsonProperty("PHONE")
    private String phone;
    @JsonProperty("CITY")
    private String city;
    @JsonProperty("NAME")
    private String name;
}
