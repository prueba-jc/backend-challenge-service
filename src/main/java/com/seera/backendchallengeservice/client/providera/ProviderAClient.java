package com.seera.backendchallengeservice.client.providera;

import com.seera.backendchallengeservice.client.ExternalClient;
import com.seera.backendchallengeservice.client.providera.model.HotelProvider;
import com.seera.backendchallengeservice.client.providera.model.Rate;
import com.seera.backendchallengeservice.client.utils.FileReaderUtils;
import com.seera.backendchallengeservice.core.model.Hotel;
import com.seera.backendchallengeservice.core.model.Room;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProviderAClient implements ExternalClient {
    @Override
    public List<Hotel> getHotelList() {
        List<HotelProvider> hotelList =
                FileReaderUtils.readList("client/providerA/hotels.json", HotelProvider.class);
        List<Rate> rateList =
                FileReaderUtils.readList("client/providerA/rates.json", Rate.class);

        return convertClientResponseToHotelList(hotelList, rateList);
    }

    private List<Hotel> convertClientResponseToHotelList(List<HotelProvider> hotelList, List<Rate> rateList) {
        Map<Integer, Hotel> hotelHashMap = new HashMap<>();

        Map<Integer, List<Rate>> rateMap = rateList.stream()
                .collect(
                        Collectors.groupingBy(Rate::getHotelId, HashMap::new, Collectors.toCollection(ArrayList::new))
                );

        for (HotelProvider hotelProvider : hotelList){
            Hotel hotel;
            if (hotelHashMap.containsKey(hotelProvider.getId())){
                hotel = hotelHashMap.get(hotelProvider.getId());
            } else {
                hotel = new Hotel(hotelProvider.getName(), hotelProvider.getCity(), new ArrayList<>());
                hotelHashMap.put(hotelProvider.getId(), hotel);
            }
            if (rateMap.containsKey(hotelProvider.getId())) {
                for (Rate rate : rateMap.get(hotelProvider.getId())) {
                    hotel.getRooms().add(new Room(rate.getName(), rate.getSize(), rate.getRate()));
                }
            }
        }
        return new ArrayList<>(hotelHashMap.values());
    }
}
