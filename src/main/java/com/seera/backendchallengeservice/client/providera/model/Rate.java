package com.seera.backendchallengeservice.client.providera.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Rate {
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Size")
    private int size;
    @JsonProperty("Rate")
    private Float rate;
    @JsonProperty("Hotel ID")
    private int hotelId;
}
