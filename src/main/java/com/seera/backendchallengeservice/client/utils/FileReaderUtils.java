package com.seera.backendchallengeservice.client.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

@Slf4j
@UtilityClass
public class FileReaderUtils {

    public <T> T readObject(String path, Class<T> clazz) {
        try {
            File file = new File(Objects.requireNonNull(FileReaderUtils.class.getClassLoader().getResource(path)).getFile());
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return objectMapper.readerFor(clazz).readValue(file);
        } catch (Exception e) {
            log.error("Error reading object.", e);
            return null;
        }
    }

    public <T> List<T> readList(String path, Class<T> clazz) {
        try {
            File file = new File(Objects.requireNonNull(FileReaderUtils.class.getClassLoader().getResource(path)).getFile());

            ObjectMapper mapper = new ObjectMapper();
            CollectionType collectionType = mapper.getTypeFactory().constructCollectionType(List.class, clazz);
            return mapper.readValue(file, collectionType);
        } catch (Exception e) {
            log.error("Error reading object.", e);
            return null;
        }
    }
}
