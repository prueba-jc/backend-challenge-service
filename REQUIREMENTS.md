Welcome to Seera's Backend Challenge. The purpose of the exercise is to build a small and simple service exposing a Hotel Room Search API. We are not interested in solving a specific problem with a given algorithm, but more in having a chance to see how you would build a real life project, how you design and code software. Details and code cleanness are important to us, so return an output that you would consider acceptable as per your quality standards.

# Hotel Room Search API

## Requests

Your API will have one single operation:

`GET /api/hotels/rooms/search`

It will accept these query parameters:

* `location`. String, optional. When provided, it will make the search to filter rooms belonging only to hotels located at the given value.
* `size`. Integer, optional. When provided, it will make the search to filter rooms big enough for the number of provided people as a value.
* `rate`. Float, optional. When provided, it will make the search to filter rooms, returning only those which are cheaper or have the same rate as the provided value.

If no query parameter is provided, the search operation will return all available hotel rooms.

## Responses

The response will be a list of hotels, each of them containing a list of rooms matching the provided filtering criteria. Only hotels with at least one matched room will be returned. Example:

```
{
   "hotels":[
      {
         "name":"Hotel Nuevo Madrid",
         "location":"Madrid",
         "rooms":[
            {
               "name":"Single Room",
               "size":1,
               "rate":98.95
            },
            {
               "name":"Double Room",
               "size":2,
               "rate":155.45
            }
         ]
      },
      {
         "name":"Hotel Amic Horizonte",
         "location":"Palma",
         "rooms":[
            {
               "name":"Single Room",
               "size":1,
               "rate":65.35
            }
         ]
      }
   ]
}
```

If the search does not match any room, the result would be an empty list of hotels:

```
{
   "hotels":[
   
   ]
}
```

Any bad request validation will return a 400 Bad Request response without body, while success search operations will return a 200 OK response.

# Data Sources

The available rooms and rates are dynamic, and can be obtained through different providers. For simplicity in this challenge, the data is provided in the form of static JSON files for two example providers, but the implementation must consume them as external resources assuming its data will be dynamic every time you consume it. There are two providers, but the implementation should be optimized to integrate to any number of external providers.

Please note the providers are independent of each other, so their output formats are different.

A small README file included in each provider folder explains the format of the data and how to read it.

# Deliverable

Please provide a working Java project for a service implementing the specified API. You can use any framework and libraries you see fit, but we recommend you use Spring Boot.

It is important you deliver a documentation file as well, with clear instructions of how we should build and run the project. Document as well all decisions you make to resolve the challenge, and why you chose any particular framework or library.

Remember to deliver a solution with the quality you consider acceptable for a real life problem. If you make any assumptions or decide to make any simplification of the requirements, remember to document it. We do understand your time to solve it could be limited, but please choose to simplify the scope of the exercise better than the quality of your output.